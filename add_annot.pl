#!/usr/bin/perl -w

use warnings;
use strict;

if ( scalar (@ARGV) < 2 ) { die "USAGE: $0 <file to be annotated> <file with the annotation information>\n\n"; }
# the second should contain at least two fields, like below
# XLOC_000018 UniRef50_A0A1B6GQM9 Uncharacterized protein n=5 Tax=Bilateria TaxID=33213 RepID=A0A1B6GQM9_9HEMI
# XLOC_000018 UniRef50_UPI00096B39F2 LOW QUALITY PROTEIN: endoplasmin n=1 Tax=Aethina tumida TaxID=116153 RepID=UPI00096B39F2
# XLOC_000018 UniRef50_B4M6G0 Uncharacterized protein n=48 Tax=Arthropoda TaxID=6656 RepID=B4M6G0_DROVI
# XLOC_000018 UniRef50_Q9VAY2 Glycoprotein 93 n=15 Tax=Sophophora TaxID=32341 RepID=Q9VAY2_DROME
#
# ...or alternatively, it can be a blast output file.

my $ANNOT_F = -2;  # which field from file2 should be transferred to file1?

if ( defined $ARGV[2] ) { # override the previous one, if $ARGV[2] is defined.
	$ANNOT_F = $ARGV[2];
}

my $GENE_F1 = 0; # this is the field of the gene name in file1
my $GENE_F2 = 0; # this is the field of the gene name in file2


open ( FILE2, $ARGV[1] ) or die;

my %annot = ();

while ( my $line = <FILE2> ) {
	chomp ($line);
	
	my @f = split (/\t/, $line);
	#$f[0] =~ s/^.*gene=(\S+).*$/$1/;
	#$f[0] =~ s/_i\d+$//;
	
	# Save the annotation
	if ( $f[ $ANNOT_F ] ) {
		$annot{ $f[ $GENE_F2 ] } .= "$f[ $ANNOT_F ] ## "; # add to already existing annotation
		#$annot{ $f[0] } .= "$f[ $ANNOT_F - 1 ] - $f[ $ANNOT_F ] ## "; # add to already existing annotation
	}
}
close (FILE2);

foreach my $gene ( keys %annot ) {
	$annot{ $gene } =~ s/ ## $//; # remove trailing ##s
}

open ( FILE1, $ARGV[0] ) or die;

my %found = ();

while ( my $line = <FILE1> ) {
	if ( $line =~ /^gene_id/ || $line =~ /^Geneid/ ) { print $line; next; }
	chomp ($line);

	my @f = split ( /\t/, $line ); # this was /\t/
	
	$found{ $f[ $GENE_F1 ] } = 0; # so that you know which loci have been found
			     # and also which loci overlap with >1 genes
	
	#print STDERR "$f[0]\n";
	
	if ( $annot{ $f[ $GENE_F1 ] } ) {
		push ( @f, $annot{ $f[ $GENE_F1 ] } );
		$found{ $f[ $GENE_F1 ] } = 1;
	}
	else {
		push ( @f, "No_annot" );
	}
	print join ( "\t", @f ), "\n";
}
