# Scripts and data in the Olive fly gastric caeca project

This git project serves as a repository for the scripts and the data used in the paper studying the gastric caeca of the olive fly, *Bactrocera oleae* (Siden-Kiamos et al. 2022).

## Usage - counts_to_tpm.pl

```
counts_to_tpm.pl <featureCounts output>
```
This script takes the raw featureCounts output and calculates TPM expression values.


## Usage - extract_specific_fasta_seqs_v3.pl
```
extract_specific_fasta_seqs_v3.pl <list of sequence names> <fasta file>
```
This script reads a fasta file (second file), and extracts the reads whose names appear in the first file


## Usage - add_annot.pl
```
add_annot.pl <file to be annotated> <file with the annotation information>
```
This script reads in a file with multiple fields (first file) and adds annotation information from another file (second file). A necessary prerequisite is that these two files have a common field.


## Support
You can send me a message here, or email me.

## License
Anyone can download any script and modify them in any way, without the need to obtain my permission.

